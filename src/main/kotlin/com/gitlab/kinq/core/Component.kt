package com.gitlab.kinq.core

import com.varabyte.kotter.runtime.MainRenderScope
import com.varabyte.kotter.runtime.RunScope
import com.varabyte.kotter.runtime.Session

/**
 * A terminal component
 */
abstract class Component<T> {
    /**
     * Allows to initialize liveVars. The preferred way to do so is :
     * ```kotlin
     * class AnyComponent : Component<Any> {
     *     private lateinit var properties: Properties
     *
     *     private interface Properties: Component.Properties {
     *         // define here your component's live properties
     *     }
     *
     *     override fun initializeProperties(session: Session) {
     *         with(session) {
     *             class AnyProperties: Properties {
     *                 // define here your properties' delegates
     *             }
     *
     *             properties = AnyProperties()
     *         }
     *     }
     * // ...
     * ```
     * You can then use your properties in other methods, and mutate them in the `onEvent` method.
     */
    abstract fun initializeProperties(session: Session)

    /**
     * Displays the prompt header, generally a question, and if the input is validated, the result.
     */
    abstract fun displayHeader(scope: MainRenderScope)

    /**
     * Displays the input block
     */
    abstract fun displayInput(scope: MainRenderScope)

    /**
     * Defines the way to manage the user input. The current implementation relies on Kotter, and the section is
     * executed with a `runUntilSignal` call, so you should end the rerender cycle with a call to `signal()`.
     */
    abstract fun onEvent(scope: RunScope, result: ValueHolder<T>)

    interface Properties {
        var validated: Boolean
    }
}
