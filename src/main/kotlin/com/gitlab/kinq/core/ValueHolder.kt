package com.gitlab.kinq.core

/**
 * A value holder, used to exchange data with the Kotter session in a null-safe fashion.
 */
class ValueHolder<T : Any?> {
    /**
     * When initialized, the holder is empty
     */
    private var holder: Option<T> = Option.None()

    /**
     * The value this class is holding. The setter should always be called before getting any value.
     */
    var value: T
        get() {
            val tempHolder = holder
            check(tempHolder is Option.Some)
            return tempHolder.value
        }
        set(value) {
            holder = Option.Some(value)
        }

    private sealed class Option<T> {
        class Some<T>(val value: T) : Option<T>()
        class None<T> : Option<T>()
    }
}
