package com.gitlab.kinq.core

data class Choice<T>(
    val displayName: String,
    val data: T,
)
