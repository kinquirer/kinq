package com.gitlab.kinq.components

import com.varabyte.kotter.foundation.input.Keys
import com.varabyte.kotter.foundation.input.onKeyPressed
import com.varabyte.kotter.foundation.liveVarOf
import com.varabyte.kotter.foundation.text.black
import com.varabyte.kotter.foundation.text.bold
import com.varabyte.kotter.foundation.text.cyan
import com.varabyte.kotter.foundation.text.green
import com.varabyte.kotter.foundation.text.red
import com.varabyte.kotter.foundation.text.text
import com.varabyte.kotter.foundation.text.textLine
import com.varabyte.kotter.runtime.MainRenderScope
import com.varabyte.kotter.runtime.RunScope
import com.varabyte.kotter.runtime.Session
import com.gitlab.kinq.core.Choice
import com.gitlab.kinq.core.Component
import com.gitlab.kinq.Kinq
import com.gitlab.kinq.core.ValueHolder
import kotlin.math.min

class CheckboxViewOptions(
    val questionMarkPrefix: String = "?",
    val cursor: String = " ❯ ",
    val nonCursor: String = "   ",
    val checked: String = "◉ ",
    val unchecked: String = "◯ ",
    val pageHint: String = "(move up and down to reveal more choices)",
    val minMessage: (Int) -> String = { min -> "min selection: $min" },
    val maxMessage: (Int) -> String = { max -> "max selection: $max" },
)

internal class CheckBoxComponent<T>(
    message: String,
    private val choices: List<Choice<T>>,
    hint: String,
    private val minNumOfSelection: Int,
    private val maxNumOfSelection: Int,
    private val pageSize: Int,
    private val viewOptions: CheckboxViewOptions,
) : Component<List<T>>() {
    private lateinit var properties: Properties

    private val listComponent = ListComponent(
        message = message,
        choices = choices,
        hint = hint,
        pageSize = pageSize,
        viewOptions = ListViewOptions(
            questionMarkPrefix = viewOptions.questionMarkPrefix,
            cursor = viewOptions.cursor,
            nonCursor = viewOptions.nonCursor,
            pageHint = viewOptions.pageHint
        )
    )

    interface Properties : ListComponent.Properties {
        var selectedIndices: Set<Int>

        var errorMessage: String
    }

    override fun initializeProperties(session: Session) {
        with(session) {
            class CheckBoxProperties : Properties {
                override var cursorIndex by liveVarOf(0)
                override var displayedIndices by liveVarOf(0 until min(pageSize, choices.size))
                override var selectedIndices by liveVarOf(emptySet<Int>())

                override var validated by liveVarOf(false)
                override var errorMessage by liveVarOf("")
            }

            properties = CheckBoxProperties()
        }
    }

    override fun displayHeader(scope: MainRenderScope) {
        listComponent.displayHeader(scope) {
            choices
                .filterIndexed { index, _ -> index in properties.selectedIndices }
                .joinToString(", ") { it.displayName }
        }
    }

    override fun displayInput(scope: MainRenderScope) {
        with(scope) {
            if (!properties.validated) {
                for (i in properties.displayedIndices) {
                    displayCursor(i)

                    displayLineAndChecked(i)
                    textLine()
                }
                displayPageHint()

                displayErrorMessage()
            }
        }
    }

    override fun onEvent(scope: RunScope, result: ValueHolder<List<T>>) {
        with(properties) {
            with(scope) {
                errorMessage = ""

                onKeyPressed {
                    when (key) {
                        Keys.UP -> listComponent.onKeyUp(properties)
                        Keys.DOWN -> listComponent.onKeyDown(properties)
                        Keys.SPACE -> onKeySpace()
                        Keys.ENTER -> onKeyEnter(result)
                    }
                }
            }
        }
    }

    private fun MainRenderScope.displayCursor(i: Int) {
        if (i == properties.cursorIndex) {
            cyan { text(viewOptions.cursor) }
        } else {
            text(viewOptions.nonCursor)
        }
    }

    private fun MainRenderScope.displayLineAndChecked(i: Int) {
        if (i in properties.selectedIndices) {
            green { text(viewOptions.checked) }
            bold { cyan { text(choices[i].displayName) } }
        } else {
            text(viewOptions.unchecked)
            text(choices[i].displayName)
        }
    }

    private fun MainRenderScope.displayPageHint() {
        if (pageSize < choices.size) {
            black(isBright = true) { textLine(viewOptions.pageHint) }
        }
    }

    private fun MainRenderScope.displayErrorMessage() {
        if (properties.errorMessage.isNotBlank()) {
            bold { red { textLine(properties.errorMessage) } }
        }
    }

    private fun Properties.onKeySpace() {
        when {
            cursorIndex in selectedIndices -> {
                selectedIndices = selectedIndices - cursorIndex
            }
            selectedIndices.size + 1 <= maxNumOfSelection -> {
                selectedIndices = selectedIndices + cursorIndex
            }
            selectedIndices.size + 1 > maxNumOfSelection -> {
                errorMessage = viewOptions.maxMessage(maxNumOfSelection)
            }
        }
    }

    private fun RunScope.onKeyEnter(result: ValueHolder<List<T>>) {
        if (properties.selectedIndices.size < minNumOfSelection) {
            properties.errorMessage = viewOptions.minMessage(minNumOfSelection)
        } else {
            result.value = choices
                .filterIndexed { index, _ -> index in properties.selectedIndices }
                .map { it.data }
            properties.validated = true
            signal()
        }
    }
}

fun <T> Kinq.promptCheckbox(
    message: String,
    choices: List<String>,
    hint: String = "",
    minNumOfSelection: Int = 0,
    maxNumOfSelection: Int = Int.MAX_VALUE,
    pageSize: Int = 10,
    viewOptions: CheckboxViewOptions = CheckboxViewOptions(),
): List<String> {
    return promptCheckboxObject(
        message = message,
        choices = choices.map { Choice(it, it) },
        hint = hint,
        minNumOfSelection = minNumOfSelection,
        maxNumOfSelection = maxNumOfSelection,
        pageSize = pageSize,
        viewOptions = viewOptions
    )
}

fun <T> Kinq.promptCheckboxObject(
    message: String,
    choices: List<Choice<T>>,
    hint: String = "",
    minNumOfSelection: Int = 0,
    maxNumOfSelection: Int = Int.MAX_VALUE,
    pageSize: Int = 10,
    viewOptions: CheckboxViewOptions = CheckboxViewOptions(),
): List<T> {
    return prompt(
        CheckBoxComponent(
            message = message,
            choices = choices,
            hint = hint,
            minNumOfSelection = minNumOfSelection,
            maxNumOfSelection = maxNumOfSelection,
            pageSize = pageSize,
            viewOptions = viewOptions
        )
    )
}
