package com.gitlab.kinq.components

import com.gitlab.kinq.Kinq
import com.gitlab.kinq.core.Component
import com.gitlab.kinq.core.ValueHolder
import com.varabyte.kotter.foundation.input.CharKey
import com.varabyte.kotter.foundation.input.Key
import com.varabyte.kotter.foundation.input.Keys
import com.varabyte.kotter.foundation.input.onInputChanged
import com.varabyte.kotter.foundation.input.onKeyPressed
import com.varabyte.kotter.foundation.liveVarOf
import com.varabyte.kotter.foundation.text.black
import com.varabyte.kotter.foundation.text.bold
import com.varabyte.kotter.foundation.text.clearInvert
import com.varabyte.kotter.foundation.text.cyan
import com.varabyte.kotter.foundation.text.green
import com.varabyte.kotter.foundation.text.invert
import com.varabyte.kotter.foundation.text.red
import com.varabyte.kotter.foundation.text.text
import com.varabyte.kotter.foundation.text.textLine
import com.varabyte.kotter.foundation.text.white
import com.varabyte.kotter.foundation.timer.addTimer
import com.varabyte.kotter.runtime.MainRenderScope
import com.varabyte.kotter.runtime.RunScope
import com.varabyte.kotter.runtime.Session
import java.math.BigDecimal
import java.time.Duration

class InputViewOptions(
    val questionMarkPrefix: String = "?",
    val validationMessage: (String) -> String = { "invalid input" },
)

internal class InputComponent(
    private val message: String,
    private val default: String,
    private val hint: String,
    private val validation: (s: String) -> Boolean,
    private val filter: (s: String) -> Boolean,
    private val transform: (s: String) -> String,
    private val viewOptions: InputViewOptions,
) : Component<String>() {
    private var pristine = true
    private lateinit var properties: Properties

    private interface Properties : Component.Properties {
        var value: String
        var cursorPos: Int

        var errorMessage: String
        var blinkOn: Boolean
    }

    override fun initializeProperties(session: Session) {
        with(session) {
            class InputProperties : Properties {
                override var value by liveVarOf("")
                override var cursorPos by liveVarOf(0)

                override var errorMessage by liveVarOf("")
                override var validated by liveVarOf(false)
                override var blinkOn by liveVarOf(true)
            }

            properties = InputProperties()
        }
    }

    override fun displayHeader(scope: MainRenderScope) {
        with(properties) {
            with(scope) {
                bold {
                    green { text("${viewOptions.questionMarkPrefix} ") }
                    white { text("$message ") }
                    if (validated) {
                        bold { cyan { textLine(value.map { transform(it.toString()) }.joinToString(separator = "")) } }
                    } else if (pristine && hint.isNotBlank()) {
                        black(isBright = true) { text(hint) }
                    }
                }
            }
        }
    }

    override fun displayInput(scope: MainRenderScope) {
        with(properties) {
            with(scope) {
                if (!validated) {
                    displayUserInputAndCaret()
                }

                displayErrorMessage()
            }
        }
    }

    override fun onEvent(scope: RunScope, result: ValueHolder<String>) {
        with(scope) {
            blinkTimer()

            onInputChanged {
                pristine = false
            }
            onKeyPressed {
                val key = this.key
                if (key is CharKey) {
                    onCharKey(key)
                } else {
                    onNonCharKey(key, result)
                }
            }
        }
    }

    private fun MainRenderScope.displayUserInputAndCaret() {
        with(properties) {
            for (i in 0..value.length) {
                if (blinkOn && i == cursorPos) {
                    invert()
                }
                if (i in value.indices) {
                    text(transform(value[i].toString()))
                } else {
                    text(" ")
                }
                clearInvert()
            }
        }
    }

    private fun MainRenderScope.displayErrorMessage() {
        if (properties.errorMessage.isNotBlank()) {
            textLine()
            bold { red { textLine(properties.errorMessage) } }
        }
    }

    private fun RunScope.blinkTimer() {
        addTimer(Duration.ofMillis(blinkDelayMs), repeat = true) { properties.blinkOn = !properties.blinkOn }
    }

    private fun onCharKey(key: CharKey) {
        with(properties) {
            pristine = false

            val nextValue = value.substring(0 until cursorPos) + key.code + value.substring(cursorPos)
            if (filter(nextValue)) {
                value = nextValue
                cursorPos++
            }
        }
    }

    private fun RunScope.onNonCharKey(key: Key, result: ValueHolder<String>) {
        with(properties) {
            when (key) {
                Keys.LEFT -> cursorPos = (cursorPos - 1).coerceIn(0..value.length)
                Keys.RIGHT -> cursorPos = (cursorPos + 1).coerceIn(0..value.length)
                Keys.HOME -> cursorPos = 0
                Keys.END -> cursorPos = value.length
                Keys.BACKSPACE -> {
                    if (cursorPos > 0) {
                        value = value.substring(0 until cursorPos - 1) + value.substring(cursorPos)
                        cursorPos--
                    }
                }
                Keys.ENTER -> {
                    if (pristine) {
                        properties.value = default
                    }
                    if (validation(properties.value)) {
                        result.value = properties.value
                        properties.validated = true
                        signal()
                    } else {
                        properties.errorMessage = viewOptions.validationMessage(properties.value)
                    }
                }
            }
        }
    }

    companion object {
        const val blinkDelayMs = 500L
    }
}

fun Kinq.promptInput(
    message: String,
    default: String = "",
    hint: String = "",
    validation: (s: String) -> Boolean = { true },
    filter: (s: String) -> Boolean = { true },
    transform: (s: String) -> String = { it },
    viewOptions: InputViewOptions = InputViewOptions(),
): String {
    return prompt(
        InputComponent(
            message = message,
            default = default,
            hint = hint,
            validation = validation,
            filter = filter,
            transform = transform,
            viewOptions = viewOptions
        )
    )
}

fun Kinq.promptInputPassword(
    message: String,
    default: String = "",
    hint: String = "",
    mask: String = "*",
): String {
    val validation: (s: String) -> Boolean = { true }
    val filter: (s: String) -> Boolean = { true }
    val transform: (s: String) -> String = { mask }

    return promptInput(
        message = message,
        default = default,
        hint = hint,
        validation = validation,
        filter = filter,
        transform = transform
    )
}

fun Kinq.promptInputNumber(
    message: String,
    default: String = "",
    hint: String = "",
    transform: (s: String) -> String = { it },
): BigDecimal {
    val validation: (s: String) -> Boolean = { it.matches("\\d+.?\\d*".toRegex()) }
    val filter: (s: String) -> Boolean = { it.matches("\\d*\\.?\\d*".toRegex()) }

    return BigDecimal(
        promptInput(
            message = message,
            default = default,
            hint = hint,
            validation = validation,
            filter = filter,
            transform = transform
        )
    )
}
