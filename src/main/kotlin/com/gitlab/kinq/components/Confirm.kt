package com.gitlab.kinq.components

import com.varabyte.kotter.foundation.input.CharKey
import com.varabyte.kotter.foundation.input.Keys
import com.varabyte.kotter.foundation.input.onKeyPressed
import com.varabyte.kotter.foundation.liveVarOf
import com.varabyte.kotter.foundation.text.bold
import com.varabyte.kotter.foundation.text.cyan
import com.varabyte.kotter.foundation.text.green
import com.varabyte.kotter.foundation.text.text
import com.varabyte.kotter.foundation.text.textLine
import com.varabyte.kotter.foundation.text.white
import com.varabyte.kotter.runtime.MainRenderScope
import com.varabyte.kotter.runtime.RunScope
import com.varabyte.kotter.runtime.Session
import com.gitlab.kinq.core.Component
import com.gitlab.kinq.Kinq
import com.gitlab.kinq.core.ValueHolder

class ConfirmViewOptions(
    val questionMarkPrefix: String = "?",
    val yesWord: String = "Yes",
    val noWord: String = "No",
    val yesKeys: List<Char> = listOf('y', 'Y', 'o', 'O'),
    val noKeys: List<Char> = listOf('n', 'N'),
)

internal class ConfirmComponent(
    private val message: String,
    private val default: Boolean,
    private val viewOptions: ConfirmViewOptions,
) : Component<Boolean>() {
    private lateinit var properties: Properties

    private interface Properties {
        var choice: Boolean
        var validated: Boolean
    }

    override fun initializeProperties(session: Session) {
        with(session) {
            class ConfirmProperties : Properties {
                override var choice by liveVarOf(default)
                override var validated by liveVarOf(false)
            }

            properties = ConfirmProperties()
        }
    }

    override fun displayHeader(scope: MainRenderScope) {
        with(properties) {
            with(scope) {
                bold {
                    green { text("${viewOptions.questionMarkPrefix} ") }
                    white { text("$message ") }
                    if (validated) {
                        cyan { text(if (choice) viewOptions.yesWord else viewOptions.noWord) }
                    }
                }
            }
        }
    }

    override fun displayInput(scope: MainRenderScope) {
        with(properties) {
            with(scope) {
                if (!validated) {
                    if (choice) {
                        text("[${viewOptions.yesWord}] ${viewOptions.noWord} ")
                    } else {
                        text(" ${viewOptions.yesWord} [${viewOptions.noWord}]")
                    }
                }

                textLine()
            }
        }
    }

    override fun onEvent(scope: RunScope, result: ValueHolder<Boolean>) {
        with(properties) {
            with(scope) {
                onKeyPressed {
                    when (key) {
                        in viewOptions.yesKeys.map { CharKey(it) } + Keys.LEFT -> {
                            choice = true
                        }
                        in viewOptions.noKeys.map { CharKey(it) } + Keys.RIGHT -> {
                            choice = false
                        }
                        Keys.ENTER -> {
                            result.value = choice
                            validated = true
                            signal()
                        }
                    }
                }
            }
        }
    }
}

fun Kinq.promptConfirm(
    message: String,
    default: Boolean = false,
    viewOptions: ConfirmViewOptions = ConfirmViewOptions(),
): Boolean {
    return prompt(
        ConfirmComponent(
            message = message,
            default = default,
            viewOptions = viewOptions
        )
    )
}
