package com.gitlab.kinq.components

import com.gitlab.kinq.Kinq
import com.gitlab.kinq.core.Choice
import com.gitlab.kinq.core.Component
import com.gitlab.kinq.core.ValueHolder
import com.varabyte.kotter.foundation.input.Keys
import com.varabyte.kotter.foundation.input.onKeyPressed
import com.varabyte.kotter.foundation.liveVarOf
import com.varabyte.kotter.foundation.text.black
import com.varabyte.kotter.foundation.text.bold
import com.varabyte.kotter.foundation.text.clearBold
import com.varabyte.kotter.foundation.text.clearColors
import com.varabyte.kotter.foundation.text.cyan
import com.varabyte.kotter.foundation.text.green
import com.varabyte.kotter.foundation.text.text
import com.varabyte.kotter.foundation.text.textLine
import com.varabyte.kotter.foundation.text.white
import com.varabyte.kotter.runtime.MainRenderScope
import com.varabyte.kotter.runtime.RunScope
import com.varabyte.kotter.runtime.Session
import kotlin.math.min

class ListViewOptions(
    val questionMarkPrefix: String = "?",
    val cursor: String = " ❯ ",
    val nonCursor: String = "   ",
    val pageHint: String = "(move up and down to reveal more choices)",
)

internal class ListComponent<T>(
    private val message: String,
    private val choices: List<Choice<T>>,
    private val hint: String,
    private val pageSize: Int,
    private val viewOptions: ListViewOptions,
) : Component<T>() {
    private lateinit var properties: Properties

    interface Properties : Component.Properties {
        var cursorIndex: Int
        var displayedIndices: IntRange
    }

    override fun initializeProperties(session: Session) {
        with(session) {
            class ListProperties : Properties {
                override var cursorIndex: Int by liveVarOf(0)
                override var displayedIndices: IntRange by liveVarOf(0 until min(pageSize, choices.size))

                override var validated by liveVarOf(false)
            }

            properties = ListProperties()
        }
    }

    fun displayHeader(scope: MainRenderScope, result: () -> String) {
        with(properties) {
            with(scope) {
                bold {
                    green { text("${viewOptions.questionMarkPrefix} ") }
                    white { text("$message ") }
                    if (validated) {
                        cyan { text(result()) }
                    } else if (hint.isNotBlank()) {
                        black(isBright = true) { text(hint) }
                    }
                }
                textLine()
            }
        }
    }

    override fun displayHeader(scope: MainRenderScope) {
        displayHeader(scope) { choices[properties.cursorIndex].displayName }
    }

    override fun displayInput(scope: MainRenderScope) {
        with(scope) {
            if (!properties.validated) {
                for (i in properties.displayedIndices) {
                    if (i == properties.cursorIndex) {
                        bold()
                        cyan()
                        text(viewOptions.cursor)
                    } else {
                        text(viewOptions.nonCursor)
                    }
                    textLine(choices[i].displayName)

                    clearBold()
                    clearColors()
                }
                if (pageSize < choices.size) {
                    black(isBright = true) { textLine(viewOptions.pageHint) }
                }
            }
        }
    }

    override fun onEvent(scope: RunScope, result: ValueHolder<T>) {
        with(scope) {
            onKeyPressed {
                when (key) {
                    Keys.UP -> onKeyUp(properties)
                    Keys.DOWN -> onKeyDown(properties)
                    Keys.ENTER -> {
                        onKeyEnter(properties, result)
                        signal()
                    }
                }
            }
        }
    }

    fun onKeyUp(properties: Properties) {
        if (properties.cursorIndex != 0) {
            properties.cursorIndex--
            if (properties.cursorIndex !in properties.displayedIndices) {
                properties.displayedIndices =
                    properties.displayedIndices.first - 1 until properties.displayedIndices.last
            }
        }
    }

    fun onKeyDown(properties: Properties) {
        if (properties.cursorIndex != choices.lastIndex) {
            properties.cursorIndex++
            if (properties.cursorIndex !in properties.displayedIndices) {
                properties.displayedIndices =
                    properties.displayedIndices.first + 1..properties.displayedIndices.last + 1
            }
        }
    }

    private fun onKeyEnter(properties: Properties, result: ValueHolder<T>) {
        result.value = choices[properties.cursorIndex].data
        properties.validated = true
    }
}

fun Kinq.promptList(
    message: String,
    choices: List<String> = emptyList(),
    hint: String = "",
    pageSize: Int = Int.MAX_VALUE,
    viewOptions: ListViewOptions = ListViewOptions(),
): String {
    return promptListObject(
        message = message,
        choices = choices.map { c -> Choice(c, c) },
        hint = hint,
        pageSize = pageSize,
        viewOptions = viewOptions
    )
}

fun <T> Kinq.promptListObject(
    message: String,
    choices: List<Choice<T>> = emptyList(),
    hint: String = "",
    pageSize: Int = Int.MAX_VALUE,
    viewOptions: ListViewOptions = ListViewOptions(),
): T {
    return prompt(
        ListComponent(
            message = message,
            choices = choices,
            hint = hint,
            pageSize = pageSize,
            viewOptions = viewOptions
        )
    )
}
