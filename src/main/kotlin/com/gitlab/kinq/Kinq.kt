package com.gitlab.kinq

import com.gitlab.kinq.core.Component
import com.gitlab.kinq.core.ValueHolder
import com.varabyte.kotter.foundation.runUntilSignal
import com.varabyte.kotter.foundation.session

/**
 * Entry point to library
 */
object Kinq {
    /**
     * Allows to display arbitrary components, producing a value in the process
     */
    fun <T> prompt(component: Component<T>): T {
        val result = ValueHolder<T>()

        session {
            component.initializeProperties(this)

            section {
                component.displayHeader(this)
                component.displayInput(this)
            }.runUntilSignal {
                component.onEvent(this, result)
            }
        }

        return result.value
    }
}
